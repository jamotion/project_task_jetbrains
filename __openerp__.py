# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Renzo on 2018-09-06.
#
{
    'name': 'Use Tasks from JetBrains IDE',
    'version': '8.0.1.0.0',
    'category': 'Project Management',
    'author': 'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Sync tasks of Odoo with JetBrains IDE',
    'images': [],
    'depends': [
        'project_task_relations',
    ],
    'data': [],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}

