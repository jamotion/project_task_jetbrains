# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 06.09.18.
#
import logging
import json
from openerp import http
from openerp.http import request
from openerp.tools.safe_eval import safe_eval


class Main(http.Controller):
    
    @http.route(['/jb/export/tasks/<token>'],
                type='http', auth="public", website=True)
    def jb_get_tasks(self, token, project=False, **kw):
        env = request.env
        if token != env['ir.config_parameter'].get_param(
                'project_task_jetbrains.token', 'notsetyet'):
            return 'Invalid Token!'
        base_url = env['ir.config_parameter'].get_param('web.base.url')
        task_url = base_url + '/web/#id={0}&model=project.task'
        search_domain = []
        if project and '-' in project:
            projects = project.split('-')
            search_domain.append(
                    ('project_id', 'in', [int(p) for p in projects]))
        elif project and int(project):
            search_domain.append(('project_id', '=', int(project)))
        tasks = env['project.task'].sudo().search(search_domain)
        result = {'tasks': []}
        for task in tasks:
            result['tasks'].append({
                'id': task.key,
                'summary': u'[{0}] {1}'.format(
                        task.task_type_id.name, task.name),
                'description': task.description,
                'updated': task.create_date,
                'created': task.write_date,
                'closed': task.stage_id.use_done,
                'issueUrl': task_url.format(task.id)
            })
        
        return json.dumps(result)
