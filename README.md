# Work with Odoo Tasks in JetBrains IDE

Sync tasks of Odoo with JetBrains IDE

## Module Configuration

Create a system parameter called **project_task_jetbrains.token** with a unique identification string.

## JetBrains Task Server Configuration

### General (part 1)

Set the Server URL to your Odoo Server and enable **Login Anonymously**:
![](docs/img/general.png)

### Commit Message

Enable **Add commit message** and set the style of commit message as shown in the screenshot: 
![](docs/img/commit.png)

### Server Configuration

Set **the Tasks List URL** as shown in the screenshot, but replace the **notsetyet** string 
with the token configured in odoo module (see above).

![](docs/img/srvconf.png)

#### Variable Mapping

Set the Variables as shown in the screenshot:

![](docs/img/parameters.png)

#### Template Variables

Open the **Template Variable** PopUp by clicking the **Manage template variables ...** button.
Add a new **Template Variable** called **project** as shown in screenshot:

![](docs/img/variable.png)

### General (part 2)

If you want to select only some projects, set the project parameter:
- for a single project add DB ID (e.g. 9)
- for multiple projects set DB IDs separated by - (e.g. 9-11-23)

![](docs/img/general2.png)